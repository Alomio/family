package platform

import (
	"database/sql"
	"log/slog"

	_ "github.com/mattn/go-sqlite3"
)

var Sqlite *sql.DB

func NewSQLite() error {
	dbName := "family.db"
	db, err := sql.Open("sqlite3", dbName)
	if err != nil {
		slog.Error("Unable to open sqlite3 database", "err", err, "filename", dbName)
		return err
	}
	Sqlite = db
	return nil
}
