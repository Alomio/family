package platform

import (
	"log/slog"
	"os"
)

var Logger *slog.Logger

// TODO: Add config to write to file or something
// checkout https://github.com/natefinch/lumberjack for log rotation
// really we want to stdout if local dev or maybe container and file if
// running on server
func SetupLogger() {
	Logger = slog.New(slog.NewJSONHandler(os.Stdout, nil))
	slog.SetDefault(Logger)
}
