package main

import (
	"fmt"
	"net/http"
    "log/slog"

	"gitlab.com/Alomio/family/app/data"
	"gitlab.com/Alomio/family/pkg/router"
	"gitlab.com/Alomio/family/platform"
)

func main() {
	platform.SetupLogger()
	platform.NewSQLite()
	mux := http.NewServeMux()
	router.AddRoutes(mux)
	if err := runMigrations(); err != nil {
		panic("could not setup data")
	}

	slog.Info("Running on http://0.0.0.0:8080/")
	if err := http.ListenAndServe(fmt.Sprintf("%s:%d", "0.0.0.0", 8080), mux); err != nil {
		panic(err)
	}
}

func runMigrations() error {
	t := data.NewSqliteTodo(platform.Sqlite)
	if err := t.Migrate(); err != nil {
		slog.Error("could not migrate todos", "err", err)
		return err
	}

	g := data.NewGroceryOperator(platform.Sqlite)
	if err := g.Migrate(); err != nil {
		slog.Error("could not migrate groceries", "err", err)
		return err
	}

	return nil
}
