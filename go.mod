module gitlab.com/Alomio/family

go 1.22.0

require (
	github.com/mattn/go-sqlite3 v1.14.22
	github.com/rs/xid v1.5.0
)
