package data

import (
	"database/sql"
	"log/slog"
	"time"

	"github.com/rs/xid"
)

// Grocery - An item on the grocery list
type Grocery struct {
	ID xid.ID
	//The name of the grocery item
	Item string
	// How many to get
	Count uint8
	//Details
	Details string
	//Whom the task is assigned
	Purchased bool
	//The time at which the task was created
	CreateAt *time.Time
}

type GroceryOperator interface {
	Migrate() error
	Add(g *Grocery) (*Grocery, error)
	All() ([]Grocery, error)
	GetByID(id xid.ID) (*Grocery, error)
	MarkBought(id *xid.ID) (*Grocery, error)
	Update(g *Grocery) (*Grocery, error)
	Delete(id xid.ID) error
}

type sqliteGroceryOperator struct {
	db *sql.DB
}

func NewGroceryOperator(d *sql.DB) *sqliteGroceryOperator {
	return &sqliteGroceryOperator{
		db: d,
	}
}

func (s *sqliteGroceryOperator) Migrate() error {
	createQuery := `CREATE TABLE IF NOT EXISTS groceries(
		grocery_id XID PRIMARY KEY,
		item TEXT NOT NULL UNIQUE,
		details TEXT,
		count INTEGER,
		purchased BOOLEAN DEFAULT false,
		created_at DATE
    );`

	if _, err := s.db.Exec(createQuery); err != nil {
		return err
	}

	return nil
}

func (s *sqliteGroceryOperator) All() ([]Grocery, error) {
	q := "SELECT grocery_id, item, details, count, purchased FROM groceries ORDER BY purchased ASC"
	rows, err := s.db.Query(q)
	if err != nil {
		return []Grocery{}, err
	}
	groceries := make([]Grocery, 0)
	for rows.Next() {
		var g Grocery
		if err = rows.Scan(&g.ID, &g.Item, &g.Details, &g.Count, &g.Purchased); err != nil {
			slog.Error("could not scan data out of DB to struct", "type", "grocery", "err", err)
			return groceries, err
		}
		groceries = append(groceries, g)
	}
	return groceries, nil
}

func (s *sqliteGroceryOperator) Add(g *Grocery) (*Grocery, error) {
	id := xid.New()
	q := "INSERT INTO groceries(grocery_id, item, details, count, purchased, created_at) VALUES(?, ?, ?, ?, ?, ?)"
	_, err := s.db.Exec(q, id, g.Item, g.Details, g.Count, false, time.Now())
	if err != nil {
		return nil, err
	}
	g.ID = id
	//Should already be false but just in case
	g.Purchased = false

	return g, nil
}

func (s *sqliteGroceryOperator) GetByID(id xid.ID) (*Grocery, error) {
	var g Grocery
	q := "SELECT grocery_id, item, details, count, purchased FROM groceries WHERE grocery_id = ?"
	if err := s.db.QueryRow(q, id).Scan(&g.ID, &g.Item, &g.Details, &g.Count, &g.Purchased); err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrNotExists
		}
		return nil, err
	}
	return &g, nil
}

func (s *sqliteGroceryOperator) MarkBought(id *xid.ID) (*Grocery, error) {
	g, err := s.GetByID(*id)
	if err != nil {
		slog.Error("Could not get the record before marking complete", "err", err)
		return nil, err
	}

	q := "UPDATE groceries SET purchased = ? where grocery_id = ?"
	if _, err := s.db.Exec(q, !g.Purchased, g.ID); err != nil {
		slog.Error("Could not get the record before marking complete", "err", err)
		return nil, err
	}

	g.Purchased = !g.Purchased
	return g, nil
}

func (s *sqliteGroceryOperator) Update(g *Grocery) (*Grocery, error) {
	q := "UPDATE groceries SET item = ?, details = ?, count = ?, purchased = ? WHERE grocer_id = ?"
	if _, err := s.db.Exec(q, g.Item, g.Details, g.Count, g.Purchased, g.ID); err != nil {
		return nil, err
	}

	return g, nil
}

func (s *sqliteGroceryOperator) Delete(id xid.ID) error {
	q := "DELETE from groceries WHERE grocery_id= ?"
	_, err := s.db.Exec(q, id)
	return err
}
