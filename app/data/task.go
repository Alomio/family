package data

import (
	"database/sql"
	"errors"
	"time"
)

// Todo - a simple task to be set as a reminder
type Todo struct {
	ID uint //xid
	//A short summary of the task
	Title string
	//A blurb detailing the task
	Details string
	//The type of task
	Category string
	//Whom the task is assigned
	Assigned string
	//The time at which the task was created
	CreateAt *time.Time
	//The time something was marked complete
	CompletedAt *time.Time
}

type TodoOperator interface {
	Migrate() error
	Add(t Todo) (*Todo, error)
	All() ([]Todo, error)
	GetByID(id uint) (*Todo, error)
	Update(t Todo) (*Todo, error)
	Delete(id uint) error
}

type InMemoryTodo struct {
	n     uint
	tasks []Todo
}

func (m *InMemoryTodo) Migrate() error {
	return nil
}

func (m *InMemoryTodo) All() ([]Todo, error) {
	return m.tasks, nil
}

func (m *InMemoryTodo) Add(t Todo) (*Todo, error) {
	t.ID = m.n
	m.n++
	m.tasks = append(m.tasks, t)
	return &t, nil
}

func (m *InMemoryTodo) Update(t Todo) (*Todo, error) {
	for k := 0; k < len(m.tasks); k++ {
		if m.tasks[k].ID == t.ID {
			m.tasks[k] = t
			return &m.tasks[k], nil
		}
	}
	return nil, ErrNotExists
}

func (m *InMemoryTodo) GetByID(id uint) (*Todo, error) {
	for _, t := range m.tasks {
		if t.ID == id {
			return &t, nil
		}
	}
	return nil, ErrNotExists
}

// remove an elmeent form a slice to ToDos
// WARNING: This modifies the original slice
// so if you don't overwrite the original you'll
// have two slices sharing memory
func remove(s []Todo, i int) []Todo {
	if i < len(s)-1 {
		s[i] = s[len(s)-1]
	}
	// s[len(s)-1] = nil if pointers to structs
	return s[:len(s)-1]
}

// small chance of a memory leak:
// https://stackoverflow.com/questions/28432658/does-go-garbage-collect-parts-of-slices/28432812#28432812
func (m *InMemoryTodo) Delete(id uint) error {
	for k := 0; k < len(m.tasks); k++ {
		if m.tasks[k].ID == id {
			m.tasks = remove(m.tasks, k)
			return nil
		}
	}
	return errors.New("Item not found")
}

func NewSqliteTodo(d *sql.DB) *sqliteTodo {
	return &sqliteTodo{
		d,
	}
}

type sqliteTodo struct {
	db *sql.DB
}

func (s *sqliteTodo) Migrate() error {
	createQuery := `CREATE TABLE IF NOT EXISTS todos(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT NOT NULL UNIQUE,
        details TEXT,
		user TEXT,
		category TEXT,
		created_at DATE,
		completed_at DATE,
		due_date DATE
    );`

	if _, err := s.db.Exec(createQuery); err != nil {
		return err
	}

	return nil
}

func (s *sqliteTodo) All() ([]Todo, error) {
	q := "SELECT id, title, details, completed_at FROM todos"
	rows, err := s.db.Query(q)
	if err != nil {
		return []Todo{}, err
	}
	tasks := make([]Todo, 0)
	for rows.Next() {
		var todo Todo
		if err = rows.Scan(&todo.ID, &todo.Title, &todo.Details, &todo.CompletedAt); err != nil {
			//platform.Logger.Error("could not scan data out of DB to struct")
			return tasks, err
		}
		tasks = append(tasks, todo)
	}
	return tasks, nil
}

func (s *sqliteTodo) Add(t Todo) (*Todo, error) {
	q := "INSERT INTO todos(title, details, created_at) VALUES(?, ?, ?)"
	res, err := s.db.Exec(q, t.Title, t.Details, time.Now())
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}
	t.ID = uint(id)

	return &t, nil
}

func (s *sqliteTodo) GetByID(id uint) (*Todo, error) {
	var t Todo
	q := "SELECT id, title, details FROM todos WHERE id = ?"
	if err := s.db.QueryRow(q, id).Scan(&t.ID, &t.Title, &t.Details); err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrNotExists
		}
		return nil, err
	}
	return &t, nil
}

func (s *sqliteTodo) Update(t Todo) (*Todo, error) {
	q := "UPDATE todos SET title = ?, details = ?, completed_at = ? WHERE id = ?"
	if _, err := s.db.Exec(q, t.Title, t.Details, t.CompletedAt, t.ID); err != nil {
		return nil, err
	}
	return &t, nil
}

func (s *sqliteTodo) Delete(id uint) error {
	q := "DELETE from todos WHERE id = ?"
	_, err := s.db.Exec(q, id)
	return err
}
