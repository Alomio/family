package controllers

import (
	"fmt"
	"net/http"
)

// Pong A niave health check
func Pong(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)

	fmt.Fprintf(w, `{"msg":"pong"}`)
}
