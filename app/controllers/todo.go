package controllers

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/Alomio/family/app/data"
	p "gitlab.com/Alomio/family/platform"
)

func ListTodos(tmpl *template.Template, q data.TodoOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		errMsg := ""
		todos, err := q.All()
		if err != nil {
			errMsg = "Could not get tasks from database, sorry."
			p.Logger.Error("could not get todo tasks from DB", "err", err.Error())
		}

		w.Header().Set("Content-Type", "text/html")
		err = tmpl.ExecuteTemplate(w, "todo.html", struct {
			Err   string
			Todos []data.Todo
		}{
			Err:   errMsg,
			Todos: todos,
		})
		if err != nil {
			p.Logger.Error("Could not execute template", err)
			fmt.Fprintf(w, "<p>Sorry, having an issue rendering this template</p>")
		}
	}
}

func GetSingleTodo(tmpl *template.Template, q data.TodoOperator, mode string) func(w http.ResponseWriter, r *http.Request) {
	template := "edit-task.html"
	if mode == "show" {
		template = "new-task.html"
	}
	return func(w http.ResponseWriter, r *http.Request) {
		todoID := r.PathValue("id")
		if todoID == "" {
			http.Error(w, "blank id", http.StatusBadRequest)
			return
		}

		id, err := strconv.ParseUint(todoID, 10, 64)
		if err != nil {
			http.Error(w, "<p>Bad Todo ID</p>", http.StatusBadRequest)
			return
		}

		t, err := q.GetByID(uint(id))
		if err != nil {
			p.Logger.Error("could not get todo tasks from DB", "err", err.Error())
			http.Error(w, "<p>Sorry, having an issue getting the task details from the db</p>", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "text/html")
		err = tmpl.ExecuteTemplate(w, template, t)
		if err != nil {
			p.Logger.Error("Could not execute template", err)
			http.Error(w, "<p>Sorry, having an issue rendering this template</p>", http.StatusInternalServerError)
		}
	}
}

func PostTodo(tmpl *template.Template, q data.TodoOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if err != nil {
			p.Logger.Error("Error parsing form", "error", err)
			fmt.Fprintf(w, "<p>ERROR! Could not parse the form data</p>")
			return
		}

		title := strings.TrimSpace(r.FormValue("title"))
		if title == "" {
			fmt.Fprintf(w, "<p class='text-warn'>Each task needs a title</p>")
		}
		details := strings.TrimSpace(r.FormValue("details"))
		t := data.Todo{
			Title:   title,
			Details: details,
		}

		t0, err := q.Add(t)
		if err != nil {
			p.Logger.Error("Error adding task to database", "err", err, "task", t)
			fmt.Fprintf(w, "<p>Sorry! Could have save this task, internal error</p>")
		}

		w.Header().Set("Content-Type", "text/html")
		err = tmpl.ExecuteTemplate(w, "new-task.html", t0)
		if err != nil {
			p.Logger.Error("Could not execute template", err)
		}
	}
}

func PutTodo(tmpl *template.Template, q data.TodoOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		todoID := r.PathValue("id")
		if todoID == "" {
			http.Error(w, "blank id", http.StatusBadRequest)
			return
		}

		id, err := strconv.ParseUint(todoID, 10, 64)
		if err != nil {
			http.Error(w, "<p>Bad Todo ID</p>", http.StatusBadRequest)
			return
		}

		err = r.ParseForm()
		if err != nil {
			p.Logger.Error("Error parsing form", "error", err)
			fmt.Fprintf(w, "<p>ERROR! Could not parse the form data</p>")
			return
		}

		title := strings.TrimSpace(r.FormValue("title"))
		if title == "" {
			fmt.Fprintf(w, "<p class='text-warn'>Each task needs a title</p>")
		}
		details := strings.TrimSpace(r.FormValue("details"))
		t := data.Todo{
			ID:      uint(id),
			Title:   title,
			Details: details,
		}

		t0, err := q.Update(t)
		if err != nil {
			p.Logger.Error("Error adding task to database", "err", err, "task", t)
			fmt.Fprintf(w, "<p>Sorry! Could have save this task, internal error</p>")
		}

		w.Header().Set("Content-Type", "text/html")
		err = tmpl.ExecuteTemplate(w, "new-task.html", t0)
		if err != nil {
			p.Logger.Error("Could not execute template", err)
		}
	}
}

func MarkDone(tmpl *template.Template, q data.TodoOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		todoID := r.PathValue("id")
		if todoID == "" {
			http.Error(w, "blank id", http.StatusBadRequest)
			return
		}

		id, err := strconv.ParseUint(todoID, 10, 64)
		if err != nil {
			http.Error(w, "<p>Bad Todo ID</p>", http.StatusBadRequest)
			return
		}

		t, err := q.GetByID(uint(id))
		if err != nil {
			p.Logger.Error("Couldn't find task to mark complete", "err", err, "id", id)
			http.Error(w, "<p>Could not find that task</p>", http.StatusBadRequest)
		}

		now := time.Now()
		t.CompletedAt = &now
		// can change to a query which must marks it complete to avoid updating other values
		t0, err := q.Update(*t)
		if err != nil {
			p.Logger.Error("Could not mark task complete", "err", err, "task", t)
			fmt.Fprintf(w, "<p>Sorry! Could not mark task complete, internal error</p>")
		}

		w.Header().Set("HX-Trigger", fmt.Sprintf("completed%d", t0.ID))
		w.Header().Set("Content-Type", "text/html")
		err = tmpl.ExecuteTemplate(w, "new-task.html", t0)
		if err != nil {
			p.Logger.Error("Could not execute template", err)
		}
	}
}

func DeleteTodo(tmpl *template.Template, q data.TodoOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		todoID := r.PathValue("id")
		if todoID == "" {
			return
		}
		id, err := strconv.ParseUint(todoID, 10, 64)
		if err != nil {
			http.Error(w, "<p>Bad Todo ID</p>", http.StatusBadRequest)
			return
		}

		if err = q.Delete(uint(id)); err != nil {
			p.Logger.Error("Unable to delete todo!", "id", todoID, "err", err)
			http.Error(w, "<p>Error trying to delete the task</p>", http.StatusInternalServerError)
			return
		}

		fmt.Fprintf(w, "")
	}
}
