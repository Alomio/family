package controllers

import (
	"html/template"
	"net/http"

	"gitlab.com/Alomio/family/platform"
)

// TODO: Change landing, login, signup to static files
func CreateIndex(tmpl *template.Template) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		err := tmpl.ExecuteTemplate(w, "index.html", struct{}{})
		if err != nil {
			platform.Logger.Error("Could not execute template", err)
		}
	}
}
