package controllers

import (
	"fmt"
	"html/template"
	"log/slog"
	"net/http"
	"strconv"
	"strings"

	"github.com/rs/xid"
	"gitlab.com/Alomio/family/app/data"
	p "gitlab.com/Alomio/family/platform"
)

func ShowGroceryListPage(tmpl *template.Template, q data.GroceryOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		errMsg := ""
		groceries, err := q.All()
		if err != nil {
			errMsg = "Could not get tasks from database, sorry."
			slog.Error("could not get todo tasks from DB", "err", err.Error())
		}

		w.Header().Set("Content-Type", "text/html")
		err = tmpl.ExecuteTemplate(w, "groceries.html", struct {
			Err       string
			Groceries []data.Grocery
		}{
			Err:       errMsg,
			Groceries: groceries,
		})
		if err != nil {
			slog.Error("Could not execute template", "err", err)
			fmt.Fprintf(w, "<p>Sorry, having an issue rendering this template</p>")
		}
	}
}

func GetSingleGrocery(tmpl *template.Template, q data.GroceryOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		itemID := r.PathValue("id")
		if itemID == "" {
			http.Error(w, "blank id", http.StatusBadRequest)
			return
		}

		id, err := xid.FromString(itemID)
		if err != nil {
			http.Error(w, "<p>Bad Todo ID</p>", http.StatusBadRequest)
			return
		}

		g, err := q.GetByID(id)
		if err != nil {
			p.Logger.Error("could not get todo tasks from DB", "err", err.Error())
			http.Error(w, "<p>Sorry, having an issue getting the task details from the db</p>", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "text/html")
		err = tmpl.ExecuteTemplate(w, "grocery.html", g)
		if err != nil {
			slog.Error("Could not execute template", "err", err, "grocery", g)
			http.Error(w, "<p>Sorry, having an issue rendering this template</p>", http.StatusInternalServerError)
		}
	}
}

func CreateGroceryItem(tmpl *template.Template, q data.GroceryOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if err != nil {
			slog.Error("Error parsing form", "err", err)
			fmt.Fprintf(w, "<p>ERROR! Could not parse the form data</p>")
			return
		}

		title := strings.TrimSpace(r.FormValue("item"))
		if title == "" {
			fmt.Fprintf(w, "<p class='text-warn'>Each task needs a title</p>")
		}

		details := strings.TrimSpace(r.FormValue("details"))

        var count uint8 = 1
		countStr := strings.TrimSpace(r.FormValue("count"))
		if countStr != "" {
			if count64, err := strconv.ParseUint(countStr, 10, 8); err == nil {
				count = uint8(count64)
			} else {
				slog.Error("couldn't parse provided count to a uint8", "err", err, "input", countStr)
			}
		}
		//store := strings.TrimSpace(r.FormValue("store"))

		g := data.Grocery{
			Item:    title,
			Details: details,
			Count:   count,
		}

		g0, err := q.Add(&g)
		if err != nil {
			slog.Error("Error adding task to database", "err", err, "grocery", g)
			fmt.Fprintf(w, "<p>Sorry! Could have save this task, internal error</p>")
		}

		w.Header().Set("Content-Type", "text/html")
		err = tmpl.ExecuteTemplate(w, "grocery.html", g0)
		if err != nil {
			slog.Error("Could not execute template", "err", err)
		}
	}
}

func PutGrocery(tmpl *template.Template, q data.GroceryOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		itemID := r.PathValue("id")
		if itemID == "" {
			http.Error(w, "blank id", http.StatusBadRequest)
			return
		}

		groceryID, err := xid.FromString(itemID)
		if err != nil {
			http.Error(w, "<p>Bad Todo ID</p>", http.StatusBadRequest)
			return
		}

		err = r.ParseForm()
		if err != nil {
			slog.Error("Error parsing form", "err", err)
			fmt.Fprintf(w, "<p>ERROR! Could not parse the form data</p>")
			return
		}

		title := strings.TrimSpace(r.FormValue("item"))
		if title == "" {
			fmt.Fprintf(w, "<p class='text-warn'>Each grocery needs a name</p>")
			return
		}

		details := strings.TrimSpace(r.FormValue("details"))

		var c uint8
		countStr := strings.TrimSpace(r.FormValue("count"))
		if countStr != "" {
			if count64, err := strconv.ParseUint(countStr, 10, 8); err == nil {
				c = uint8(count64)
			} else {
				slog.Error("couldn't parse provided count to a uint8", "err", err, "input", countStr)
			}
		}

		purchasedStr := r.FormValue("purchased")
		purchased, err := strconv.ParseBool(purchasedStr)
		if err != nil {
			slog.Error("Could not parse boolean value from form input!", "err", err, "form value", purchasedStr)
			fmt.Fprintf(w, "<p>Oops! Couldn't understand the input for if this item was purchased or not!\n%s</p>", err)
			return
		}

		g := data.Grocery{
			ID:        groceryID,
			Item:      title,
			Details:   details,
			Count:     c,
			Purchased: purchased,
		}

		_, err = q.Update(&g)
		if err != nil {
			slog.Error("Error adding grocery to database", "err", err, "grocery", g)
			fmt.Fprintf(w, "<p>Sorry! Could have save this grocery to the list!, internal error</p>")
			return
		}

		w.Header().Set("Content-Type", "text/html")
		err = tmpl.ExecuteTemplate(w, "grocery.html", g)
		if err != nil {
			p.Logger.Error("Could not execute grocery template", err)
		}
	}
}

func ToggleBought(tmpl *template.Template, q data.GroceryOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		itemID := r.PathValue("id")
		if itemID == "" {
			http.Error(w, "blank id", http.StatusBadRequest)
			return
		}

		groceryID, err := xid.FromString(itemID)
		if err != nil {
			http.Error(w, "<p>Bad Todo ID</p>", http.StatusBadRequest)
			return
		}

		g, err := q.MarkBought(&groceryID)
		if err != nil {
			slog.Error("Could not mark grocery item purchased", "err", err, "groceryID", groceryID)
			fmt.Fprintf(w, "<p>Sorry! Could not mark task complete, internal error</p>")
			return
		}

		w.Header().Set("Content-Type", "text/html")
		err = tmpl.ExecuteTemplate(w, "grocery.html", g)
		if err != nil {
			slog.Error("Could not execute template", "err", err)
		}
	}
}

func DeleteGrocery(tmpl *template.Template, q data.GroceryOperator) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		itemID := r.PathValue("id")
		if itemID == "" {
			http.Error(w, "blank id", http.StatusBadRequest)
			return
		}

		groceryID, err := xid.FromString(itemID)
		if err != nil {
			http.Error(w, "<p>Bad Item ID</p>", http.StatusBadRequest)
			return
		}

		if err = q.Delete(groceryID); err != nil {
			p.Logger.Error("Unable to delete grocery", "id", groceryID, "err", err)
			http.Error(w, "<p>Error trying to delete the item</p>", http.StatusInternalServerError)
			return
		}

		fmt.Fprintf(w, "")
	}
}
