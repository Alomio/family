package controllers

import (
	"html/template"
	"net/http"

	p "gitlab.com/Alomio/family/platform"
)

func ShowProfile(tmpl *template.Template) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		err := tmpl.ExecuteTemplate(w, "profile.html", struct{}{})
		if err != nil {
			p.Logger.Error("Could not execute template", err)
			http.Error(w, "Error loading template", http.StatusInternalServerError)
		}
	}
}
