/**
 * Apply a listener to the theme selector which is present in the header
 * nav bar. If a theme is selected, it's added to local storage.
 */
const addThemeListener = () => {
	document
		.querySelector("#theme-select")
		.addEventListener("change", function () {
			const theme = this.value ? this.value : "retro";
			const themedElements = document.querySelector("[data-theme]");
			if (themedElements) {
				themedElements.dataset.theme = theme;
				localStorage.setItem("theme", theme);
			}
		});
};

/**
 * When the page loads, check if there was a pre-applied theme in local storage
 * and set it if it exists.
 */
const applyThemeOnLoad = () => {
	const theme = localStorage.getItem("theme");
	if (theme) {
		const themedElements = document.querySelector("[data-theme]");
		if (themedElements) {
			themedElements.dataset.theme = theme;
		}
	}
};

applyThemeOnLoad();
addThemeListener();
