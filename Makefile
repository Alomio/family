build:
	npm run build
	go build -o server main.go

run:
	npm run build
	go run main.go
