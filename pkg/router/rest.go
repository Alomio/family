package router

import (
	"net/http"

	"gitlab.com/Alomio/family/app/controllers"
)

func addRESTRoutes(r *http.ServeMux) {
	r.HandleFunc("GET /api/v1/ping", controllers.Pong)
}
