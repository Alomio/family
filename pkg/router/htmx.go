package router

import (
	"html/template"
	"net/http"

	"gitlab.com/Alomio/family/app/controllers"
	"gitlab.com/Alomio/family/app/data"
	p "gitlab.com/Alomio/family/platform"
)

func addHTMXRoutes(r *http.ServeMux) {
	tmpl := template.Must(template.New("website").Funcs(template.FuncMap{
		"myFunc": func(n uint8) []uint8 {
			s := make([]uint8, n)
			for i := uint8(0); i < n; i++ {
				s[i] = i
			}
			return s
		},
	}).ParseGlob("templates/*.html"))

	r.HandleFunc("GET /", controllers.CreateIndex(tmpl))
	r.HandleFunc("GET /profile", controllers.ShowProfile(tmpl))

	//t := data.InMemoryTodo{}
	t := data.NewSqliteTodo(p.Sqlite)
	r.HandleFunc("GET /todos", controllers.ListTodos(tmpl, t))
	r.HandleFunc("GET /todos/{id}", controllers.GetSingleTodo(tmpl, t, "show"))
	r.HandleFunc("GET /todos/{id}/edit", controllers.GetSingleTodo(tmpl, t, "edit"))
	r.HandleFunc("PUT /todos/{id}/done", controllers.MarkDone(tmpl, t))
	r.HandleFunc("POST /todos", controllers.PostTodo(tmpl, t))
	r.HandleFunc("PUT /todos/{id}", controllers.PutTodo(tmpl, t))
	r.HandleFunc("DELETE /todos/{id}", controllers.DeleteTodo(tmpl, t))

	g := data.NewGroceryOperator(p.Sqlite)
	r.HandleFunc("GET /groceries", controllers.ShowGroceryListPage(tmpl, g))
	r.HandleFunc("GET /groceries/{id}", controllers.GetSingleGrocery(tmpl, g))
	r.HandleFunc("PUT /groceries/{id}/done", controllers.ToggleBought(tmpl, g))
	r.HandleFunc("POST /groceries", controllers.CreateGroceryItem(tmpl, g))
	r.HandleFunc("PUT /groceries/{id}", controllers.PutGrocery(tmpl, g))
	r.HandleFunc("DELETE /groceries/{id}", controllers.DeleteGrocery(tmpl, g))
}
