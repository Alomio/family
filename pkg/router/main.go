package router

import (
	"net/http"
)

func AddRoutes(r *http.ServeMux) {
	addRESTRoutes(r)
	addHTMXRoutes(r)

	s := http.StripPrefix("/static/", http.FileServer(http.Dir("./static/")))
	r.Handle("GET /static/{pathname...}", s)
}
