# Showboat
An application to help facilitate young adults reaching out to college coaches
and present their information for recruiting

## Features
 - email coaches

## Tech
 - Sqlite
 - Go
 - HTMX
 - TailwindCSS
